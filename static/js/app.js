var App = angular.module('App', ['ngRoute'])
    .config(function($routeProvider) {
       $routeProvider
        .when('/', {
            controller: 'MainController',
            templateUrl: 'static/partials/main.html'
        })
        .when('/login', {
            controller: 'LoginController',
            templateUrl: 'static/partials/login.html'
        })
        .when('/register', {
            controller: 'RegisterController',
            templateUrl: 'static/partials/register.html'
        })
        .otherwise({
            redirectTo: '/'
        });
    });
