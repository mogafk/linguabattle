(function() {

    angular.module('App')
        .controller('LoginController', function($scope, $location, AuthService) {
            $scope.doLogin = function($event) {
                if ($event) {
                    $event.preventDefault();
                }
                AuthService.login($scope.login, $scope.password)
                    .then(function() {
                        $location.path('/');
                    },
                    function(message) {
                        $scope.message = message;
                        console.log("Auth failed");
                    });

            };
        });

})();
