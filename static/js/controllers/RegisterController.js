(function() {

    angular.module('App')
        .controller('RegisterController', function($scope, $location, AuthService) {
            $scope.doRegister = function($event) {
                if ($event) {
                    $event.preventDefault();
                }
                AuthService.register($scope.login, $scope.password)
                    .then(function() {
                        $location.path('/');
                    },
                    function(message) {
                        $scope.message = message;
                    });
            };
        });

})();
