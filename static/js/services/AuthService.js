(function() {

    angular.module('App')
        .factory('AuthService', ['$q', '$timeout', '$http', function($q, $timeout, $http) {
            var token = null;
            var isLoggedIn = function() {
                return !! token;
            };
            var login = function(login, password) {
                var deffered = $q.defer();
                $http.post('/login', {
                    login: login,
                    password: password
                }).success(function(data, status) {
                    if (status == 200 && data.response) {
                        token = data.access_token;
                        deffered.resolve();
                    }
                    token = null;
                    deffered.reject();
                }).error(function(data) {
                    token = null;
                    deffered.reject();
                });
                return deffered.promise;
            };
            var logout = function() {
                token = null;
            };

            var register = function(login, password) {
                var deffered = $q.defer();
                $http.post('/register', {
                    login: login,
                    password: password
                }).success(function(data, status) {
                    if (status == 200) {
                        deffered.resolve();
                    }
                    token = null;
                    deffered.reject();
                }).error(function(data) {
                    deffered.reject();
                });
                return deffered.promise;
            };
            return {
                isLoggedIn: isLoggedIn,
                login: login,
                logout: logout,
                register: register
            };
        }]);
})();
