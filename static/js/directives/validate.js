    (function() {
    angular.module("App")
        //.directive("email", function(){
        //    var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+/=?^_`{|}~.-]+@example\.com$/i;
        //
        //      return {
        //          require: 'ngModel',
        //          restrict: '',
        //          link: function (scope, elm, attrs, ctrl) {
                      //if (ctrl && ctrl.$validators.email) {
                      //
                          //ctrl.$validators.email = function (modelValue) {
                          //    return ctrl.$isEmpty(modelValue) || EMAIL_REGEXP.test(modelValue);
                          //};
                      //}
                  //}
              //}
        //})
        .directive("pass", function(){
            return {
                require: "ngModel",
                link: function(scope, elem, attrs, ctrl){
                    var PASS_REGEXP = /^[A-z0-9]*$/;

                    ctrl.$validators.pass = function(modelValue){
                        return ctrl.$isEmpty(modelValue) || PASS_REGEXP.test(modelValue);
                    }
                }
            }
        })
        .directive("repeatPass", function($compile){
            return {
                require: "ngModel",
                scope: {
                    pass: "=repeatPass"
                },
                link: function(scope, elem, attrs, ctrl){
                    ctrl.$validators.repeat = function(modelValue){
                        return modelValue == scope.pass;
                    };

                    scope.$watch("pass", function(){
                        ctrl.$validate();
                    });
                }
            }
        })
})();

    //angular.module('App', [])
  //.controller('Controller', function($scope) {
    //$scope.savedUser = null;
    //$scope.save = function(user) {
      //$scope.savedUser = angular.copy(user);
    //};
  //});

    //{
    //    "maxlength":
    //    [{
    //        "$viewValue": "kjljkljlkjkjlkjlkjlkjklj",
    //        "$validators": {},
    //        "$asyncValidators": {},
    //        "$parsers": [],
    //        "$formatters": [null],
    //        "$viewChangeListeners": [],
    //        "$untouched": false,
    //        "$touched": true,
    //        "$pristine": false,
    //        "$dirty": true,
    //        "$valid": false,
    //        "$invalid": true,
    //        "$error": {"maxlength": true},
    //        "$name": "login",
    //        "$options": {"updateOn": "blur", "updateOnDefault": false}
    //    }]
    //}