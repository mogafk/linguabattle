#!/usr/bin/env python3

from app import app
import unittest
from tornado.testing import AsyncHTTPTestCase, main

class MainTestCase(AsyncHTTPTestCase):
    def get_app(self):
        return app
    
    def test_home(self):
        resp = self.fetch('/')
        self.assertEqual(resp.code, 200)

    def test_login(self):
        resp = self.fetch('/login')
        self.assertEqual(resp.code, 405)

        resp = self.fetch('/login', method='POST', body='{"login": "", "password": ""}')
        self.assertEqual(resp.code, 400)


if __name__ == '__main__':
    unittest.main()

